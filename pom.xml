<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    
    <parent>
        <groupId>org.sonatype.oss</groupId>
        <artifactId>oss-parent</artifactId>
        <version>7</version>
    </parent>
    
    <groupId>com.atlassian.jgitflow</groupId>
    <artifactId>jgit-flow</artifactId>
    <version>0.22-SNAPSHOT</version>
    <modules>
        <module>jgit-flow-core</module>
    </modules>
    <name>JGit-Flow Parent pom</name>
    <description>The JGit Flow parent pom</description>
    <packaging>pom</packaging>
    
    <url>https://bitbucket.org/atlassian/jgit-flow/wiki</url>
    
    <licenses>
        <license>
            <name>Apache 2</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
            <distribution>repo</distribution>
            <comments>A business-friendly OSS license</comments>
        </license>
    </licenses>

    <organization>
        <name>Atlassian</name>
        <url>http://www.altassian.com/</url>
    </organization>
    
    <developers>
        <developer>
            <name>Jonathan Doklovic</name>
            <organization>Atlassian</organization>
            <email>doklovic@atlassian.com</email>
        </developer>
    </developers>
    
    <scm>
        <connection>scm:git:ssh://git@bitbucket.org/atlassian/jgit-flow.git</connection>
        <developerConnection>scm:git:ssh://git@bitbucket.org/atlassian/jgit-flow.git</developerConnection>
        <url>https://bitbucket.org/atlassian/jgit-flow</url>
        <tag>HEAD</tag>
    </scm>

    <distributionManagement>
        <site>
            <id>atlassian-documentation</id>
            <url>dav:https://docs.atlassian.com/${project.artifactId}/${project.version}</url>
        </site>
    </distributionManagement>
    
    <!--
    <dependencies>
        <dependency>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-release-plugin</artifactId>
            <version>2.3.2</version>
        </dependency>
    </dependencies>
    -->
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.eclipse.jgit</groupId>
                <artifactId>org.eclipse.jgit</artifactId>
                <version>${jgitVersion}</version>
                <exclusions>
                    <exclusion>
                        <groupId>com.jcraft</groupId>
                        <artifactId>jsch</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>
            <dependency>
                <groupId>org.apache.servicemix.bundles</groupId>
                <artifactId>org.apache.servicemix.bundles.jsch</artifactId>
                <version>0.1.49_1</version>
            </dependency>
            <dependency>
                <groupId>com.google.guava</groupId>
                <artifactId>guava</artifactId>
                <version>${guavaVersion}</version>
            </dependency>
            <dependency>
                <groupId>junit</groupId>
                <artifactId>junit</artifactId>
                <version>4.10</version>
            </dependency>
            <dependency>
                <groupId>commons-io</groupId>
                <artifactId>commons-io</artifactId>
                <version>2.4</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.felix</groupId>
                    <artifactId>maven-bundle-plugin</artifactId>
                    <version>2.3.7</version>
                    <extensions>true</extensions>
                    <configuration>
                        <archive>
                            <manifest>
                                <addDefaultImplementationEntries>true</addDefaultImplementationEntries>
                            </manifest>
                        </archive>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
        
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <!-- v 2.4 has a bug with localCheckout. will be fixed in 2.4.1 -->
                <version>2.3.2</version>
                <configuration>
                    <autoVersionSubmodules>true</autoVersionSubmodules>
                    <goals>deploy</goals>
                    <pushChanges>false</pushChanges>
                    <localCheckout>true</localCheckout>
                    <tagNameFormat>mvn-v@{project.version}</tagNameFormat>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>release-sign-artifacts</id>
            <activation>
                <property>
                    <name>performRelease</name>
                    <value>true</value>
                </property>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-gpg-plugin</artifactId>
                        <version>1.4</version>
                        <executions>
                            <execution>
                                <id>sign-artifacts</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>

        <profile>
            <!-- to run this, do a normal release and then run: mvn validate -Patlas-deploy -Dversion=1.0-alpha27 -->
            <id>atlas-deploy</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-deploy-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>deploy-to-atlas</id>
                                <phase>validate</phase>
                                <goals>
                                    <goal>deploy-file</goal>
                                </goals>
                                <configuration>
                                    <repositoryId>atlassian-private</repositoryId>
                                    <url>https://maven.atlassian.com/content/repositories/atlassian-private/</url>
                                    <file>${basedir}/target/${project.artifactId}-${version}.jar</file>
                                    <pomFile>${basedir}/target/${project.artifactId}-${version}.pom</pomFile>
                                    <javadoc>${basedir}/target/${project.artifactId}-${version}-javadoc.jar</javadoc>
                                    <sources>${basedir}/target/${project.artifactId}-${version}-sources.jar</sources>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
            <distributionManagement>
                <repository>
                    <id>atlassian-private</id>
                    <name>Private Atlassian Repo</name>
                    <url>https://maven.atlassian.com/content/repositories/atlassian-private/</url>
                </repository>
            </distributionManagement>
        </profile>
    </profiles>
    
    <properties>
        <guavaVersion>10.0.1</guavaVersion>
        <jgitVersion>3.0.0.201306101825-r</jgitVersion>
    </properties>
</project>